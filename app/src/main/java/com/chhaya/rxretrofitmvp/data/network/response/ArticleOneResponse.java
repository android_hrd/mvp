package com.chhaya.rxretrofitmvp.data.network.response;

import com.chhaya.rxretrofitmvp.data.network.entities.ArticleEntity;
import com.chhaya.rxretrofitmvp.data.network.entities.PaginationEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleOneResponse {
    @Expose
    @SerializedName("PAGINATION")
    private PaginationEntity pagination;
    @Expose
    @SerializedName("DATA")
    private ArticleEntity data;
    @Expose
    @SerializedName("MESSAGE")
    private String message;
    @Expose
    @SerializedName("CODE")
    private String code;

    public PaginationEntity getPagination() {
        return pagination;
    }

    public void setPagination(PaginationEntity pagination) {
        this.pagination = pagination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArticleEntity getData() {
        return data;
    }

    public void setData(ArticleEntity data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ArticleOneResponse{" +
                "pagination=" + pagination +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
