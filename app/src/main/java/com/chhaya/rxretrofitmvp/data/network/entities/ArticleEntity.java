package com.chhaya.rxretrofitmvp.data.network.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleEntity {

    @Expose
    @SerializedName("IMAGE")
    private String image;
    @Expose
    @SerializedName("CREATED_DATE")
    private String createdDate;
    @Expose
    @SerializedName("DESCRIPTION")
    private String description;
    @Expose
    @SerializedName("TITLE")
    private String title;
    @Expose
    @SerializedName("ID")
    private int id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ArticleEntity{" +
                "image='" + image + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}
