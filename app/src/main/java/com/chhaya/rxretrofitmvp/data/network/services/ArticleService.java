package com.chhaya.rxretrofitmvp.data.network.services;

import com.chhaya.rxretrofitmvp.data.network.response.ArticleOneResponse;
import com.chhaya.rxretrofitmvp.data.network.response.ArticleResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {
    @GET("v1/api/articles")
    Call<ArticleResponse> fetch(@Query("page") int page, @Query("limit") int limit);

    @GET("v1/api/articles/{id}")
    Single<ArticleOneResponse> findOne(@Path("id") int id);

    @GET("v1/api/articles")
    Observable<ArticleResponse> findAll(@Query("page") int page, @Query("limit") int limit);
}
