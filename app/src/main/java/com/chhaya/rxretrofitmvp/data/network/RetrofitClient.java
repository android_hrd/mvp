package com.chhaya.rxretrofitmvp.data.network;

import com.chhaya.rxretrofitmvp.data.network.services.ArticleService;

import io.reactivex.internal.schedulers.RxThreadFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String BASE_URL = "http://www.api-ams.me/";
    private static final RetrofitClient ourInstance = new RetrofitClient();

    public static RetrofitClient getInstance() {
        return ourInstance;
    }

    private RetrofitClient(){}

    public synchronized ArticleService getArticleService() {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ArticleService.class);
    }

}
