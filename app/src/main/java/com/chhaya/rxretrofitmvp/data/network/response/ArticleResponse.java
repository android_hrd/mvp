package com.chhaya.rxretrofitmvp.data.network.response;

import com.chhaya.rxretrofitmvp.data.network.entities.ArticleEntity;
import com.chhaya.rxretrofitmvp.data.network.entities.PaginationEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleResponse {

    @Expose
    @SerializedName("PAGINATION")
    private PaginationEntity pagination;
    @Expose
    @SerializedName("DATA")
    private List<ArticleEntity> data;
    @Expose
    @SerializedName("MESSAGE")
    private String message;
    @Expose
    @SerializedName("CODE")
    private String code;

    public PaginationEntity getPagination() {
        return pagination;
    }

    public void setPagination(PaginationEntity pagination) {
        this.pagination = pagination;
    }

    public List<ArticleEntity> getData() {
        return data;
    }

    public void setData(List<ArticleEntity> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ArticleResponse{" +
                "pagination=" + pagination +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}

