package com.chhaya.rxretrofitmvp.utils;

import android.content.Context;
import android.widget.ProgressBar;

public class CommonUtils {
    public static ProgressBar progressBar(Context context) {
        return new ProgressBar(context, null, android.R.attr.progressBarStyleSmall);
    }
}
