package com.chhaya.rxretrofitmvp.ui.article;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.chhaya.rxretrofitmvp.R;
import com.chhaya.rxretrofitmvp.data.network.entities.ArticleEntity;
import com.chhaya.rxretrofitmvp.ui.article.mvp.ArticleMvp;
import com.chhaya.rxretrofitmvp.ui.article.mvp.ArticlePresenter;
import com.chhaya.rxretrofitmvp.ui.base.BaseActivity;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ArticleActivity extends BaseActivity implements ArticleMvp.View {

    private ArticlePresenter<ArticleMvp.View> articlePresenter;
    private List<ArticleEntity> articleList;
    private ArticleListAdapter articleListAdapter;
    private RecyclerView rcv;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        rcv = findViewById(R.id.rcv_article);
        articleList = new ArrayList<>();

        articlePresenter = new ArticlePresenter<>();
        articlePresenter.setMvpView(this);
        //articlePresenter.onFetch(1, 15);
        //articlePresenter.findOne(120416);
        articlePresenter.findAll(1, 15);
    }

    @Override
    public void onFetching() {
        Toast.makeText(this, "Fetching is ready", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoading() {
        Toast.makeText(this, "On Loading is ready", Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayArticlesOnRcv(List<ArticleEntity> articleList) {
        articleListAdapter = new ArticleListAdapter(articleList);
        layoutManager = new LinearLayoutManager(this);
        rcv.setAdapter(articleListAdapter);
        rcv.setLayoutManager(layoutManager);
        articleListAdapter.notifyDataSetChanged();
    }
}
