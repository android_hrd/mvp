package com.chhaya.rxretrofitmvp.ui.base;

import android.view.View;

import com.chhaya.rxretrofitmvp.ui.login.mvp.LoginMvp;

public interface MvpPresenter<V extends MvpView> {
    void setMvpView(V mvpView);
}
