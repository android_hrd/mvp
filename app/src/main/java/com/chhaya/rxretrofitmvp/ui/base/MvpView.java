package com.chhaya.rxretrofitmvp.ui.base;

import androidx.annotation.StringRes;

public interface MvpView {
    void showLoading();
    void hideLoading();
    void showMessage(String msg);
}
