package com.chhaya.rxretrofitmvp.ui.article;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chhaya.rxretrofitmvp.R;
import com.chhaya.rxretrofitmvp.data.network.entities.ArticleEntity;

import java.util.ArrayList;
import java.util.List;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.MyViewHolder> {

    private List<ArticleEntity> articles;

    public ArticleListAdapter(List<ArticleEntity> articles) {
        this.articles = new ArrayList<>();
        this.articles = articles;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_item_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageArticle.setImageResource(R.drawable.a);
        holder.textTitle.setText(articles.get(position).getTitle());
        holder.textDate.setText(articles.get(position).getCreatedDate());
        holder.textDesc.setText(articles.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageArticle;
        TextView textTitle, textDate, textDesc;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageArticle = itemView.findViewById(R.id.image_article);
            textTitle = itemView.findViewById(R.id.text_title);
            textDate = itemView.findViewById(R.id.text_date);
            textDesc = itemView.findViewById(R.id.text_desc);
        }
    }
}
