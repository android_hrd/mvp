package com.chhaya.rxretrofitmvp.ui.login.mvp;

import androidx.annotation.StringRes;

import com.chhaya.rxretrofitmvp.data.network.entities.User;
import com.chhaya.rxretrofitmvp.ui.base.MvpPresenter;
import com.chhaya.rxretrofitmvp.ui.base.MvpView;

public interface LoginMvp {

    interface View extends MvpView {
        void onLoginSuccess(@StringRes int strId);
        void onLoginError(@StringRes int strId);
        void openArticleActivity();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void onAttemptLogin(User user);
    }

    interface Interactor {
        void authenticate(User user, LoginCallback callback);
        interface LoginCallback {
            void onSuccess(@StringRes int strId);
            void onError(@StringRes int strId);
        }
    }

}
