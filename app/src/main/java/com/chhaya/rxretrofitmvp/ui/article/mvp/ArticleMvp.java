package com.chhaya.rxretrofitmvp.ui.article.mvp;

import com.chhaya.rxretrofitmvp.callback.InteractorResponse;
import com.chhaya.rxretrofitmvp.data.network.entities.ArticleEntity;
import com.chhaya.rxretrofitmvp.data.network.response.ArticleResponse;
import com.chhaya.rxretrofitmvp.ui.base.MvpPresenter;
import com.chhaya.rxretrofitmvp.ui.base.MvpView;
import com.chhaya.rxretrofitmvp.ui.login.mvp.LoginMvp;

import java.util.List;

public interface ArticleMvp {

    interface View extends MvpView {
        void displayArticlesOnRcv(List<ArticleEntity> articleList);
        void onFetching();
        void onLoading();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void onFetch(int page, int limit);
        void findOne(int id);
        void findAll(int page, int limit);
    }

    interface Interactor {
        void fetch(int page, int limit, InteractorCallback callback);
        void findOne(int id, InteractorCallback callback);
        void findAll(int page, int limit, InteractorResponse<ArticleResponse> callback);
        void onDestroy();
        interface InteractorCallback {
            void onSuccess(ArticleResponse articleResponse);
            void onError(String message);
        }
    }

}
