package com.chhaya.rxretrofitmvp.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.chhaya.rxretrofitmvp.R;
import com.chhaya.rxretrofitmvp.data.network.entities.User;
import com.chhaya.rxretrofitmvp.ui.article.ArticleActivity;
import com.chhaya.rxretrofitmvp.ui.base.BaseActivity;
import com.chhaya.rxretrofitmvp.ui.login.mvp.LoginMvp;
import com.chhaya.rxretrofitmvp.ui.login.mvp.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginMvp.View {

    @BindView(R.id.edit_mail)
    EditText editMail;
    @BindView(R.id.edit_pwd)
    EditText editPassword;
    @BindView(R.id.button_login)
    Button btnLogin;

    private LoginPresenter<LoginMvp.View> loginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        configureEvent();

        loginPresenter = new LoginPresenter<>();
        loginPresenter.setMvpView(this);
    }

    private void configureEvent() {
        btnLogin.setOnClickListener(v -> {
            String name = editMail.getText().toString();
            String pwd = editPassword.getText().toString();
            loginPresenter.onAttemptLogin(new User(name, pwd));
        });
    }

    @Override
    public void onLoginSuccess(int strId) {
        //Snackbar.make(btnLogin, R.string.msg_login_success, Snackbar.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent articleIntent = new Intent(LoginActivity.this, ArticleActivity.class);
                startActivity(articleIntent);
            }
        }, 2000);
    }

    @Override
    public void onLoginError(int strId) {
        //Snackbar.make(btnLogin, R.string.msg_login_fail, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openArticleActivity() {

    }
}
