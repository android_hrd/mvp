package com.chhaya.rxretrofitmvp.ui.article.mvp;

import android.util.Log;

import com.chhaya.rxretrofitmvp.callback.InteractorResponse;
import com.chhaya.rxretrofitmvp.data.network.response.ArticleResponse;
import com.chhaya.rxretrofitmvp.ui.base.BasePresenter;

public class ArticlePresenter<V extends ArticleMvp.View> extends BasePresenter<V> implements ArticleMvp.Presenter<V> {

    private ArticleInteractor interactor;

    public ArticlePresenter() {
        interactor = new ArticleInteractor();
    }

    @Override
    public void onFetch(int page, int limit) {
        //getMvpView().showLoading();
        interactor.fetch(page, limit, new ArticleMvp.Interactor.InteractorCallback() {
            @Override
            public void onSuccess(ArticleResponse articleResponse) {
                getMvpView().displayArticlesOnRcv(articleResponse.getData());
                //getMvpView().hideLoading();
            }

            @Override
            public void onError(String message) {
                //getMvpView().hideLoading();
            }
        });
    }

    @Override
    public void findOne(int id) {
        /*getMvpView().showLoading();
        interactor.findOne(id, new ArticleMvp.Interactor.InteractorCallback() {
            @Override
            public void onSuccess(ArticleResponse articleResponse) {
                getMvpView().displayArticlesOnRcv(articleResponse.getData());
            }

            @Override
            public void onError(String message) {
                getMvpView().hideLoading();
            }
        });*/
    }

    @Override
    public void findAll(int page, int limit) {
        interactor.findAll(page, limit, new InteractorResponse<ArticleResponse>() {
            @Override
            public void onSuccess(ArticleResponse response) {
                getMvpView().displayArticlesOnRcv(response.getData());
            }

            @Override
            public void onComplete(String message) {
                Log.d("ArticleMessge", message);
            }

            @Override
            public void onError(String error) {
                Log.d("ArticleMessge", error);
            }
        });
    }


}
