package com.chhaya.rxretrofitmvp.ui.base;

import com.chhaya.rxretrofitmvp.ui.article.mvp.ArticleMvp;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {
    private V mvpView;

    public V getMvpView() {
        return mvpView;
    }

    @Override
    public void setMvpView(V mvpView) {
        this.mvpView = mvpView;
    }

}
