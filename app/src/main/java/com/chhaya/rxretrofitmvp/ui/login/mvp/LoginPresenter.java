package com.chhaya.rxretrofitmvp.ui.login.mvp;

import android.os.Handler;

import com.chhaya.rxretrofitmvp.R;
import com.chhaya.rxretrofitmvp.data.network.entities.User;
import com.chhaya.rxretrofitmvp.ui.base.BasePresenter;

public class LoginPresenter<V extends LoginMvp.View> extends BasePresenter<V> implements LoginMvp.Presenter<V> {

    private LoginMvp.Interactor interactor;

    public LoginPresenter() {
        interactor = new LoginInteractor();
    }

    @Override
    public void onAttemptLogin(User user) {
        getMvpView().showLoading();
        interactor.authenticate(user, new LoginMvp.Interactor.LoginCallback() {
            @Override
            public void onSuccess(int strId) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getMvpView().onLoginSuccess(R.string.msg_login_success);
                        getMvpView().hideLoading();
                    }
                },2000);
            }

            @Override
            public void onError(int strId) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getMvpView().onLoginError(R.string.msg_login_fail);
                        getMvpView().hideLoading();
                    }
                }, 2000);
            }
        });
    }

}
