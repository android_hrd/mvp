package com.chhaya.rxretrofitmvp.ui.login.mvp;

import com.chhaya.rxretrofitmvp.R;
import com.chhaya.rxretrofitmvp.data.network.entities.User;

public class LoginInteractor implements LoginMvp.Interactor {

    final static String DEFAULT_USERNAME = "system";
    final static String DEFAULT_PASSWORD = "123";

    @Override
    public void authenticate(User user, LoginCallback callback) {
        if (DEFAULT_USERNAME.equals(user.getName()) && DEFAULT_PASSWORD.equals(user.getPassword())) {
            callback.onSuccess(R.string.msg_login_success);
        } else {
            callback.onError(R.string.msg_login_fail);
        }
    }
}
