package com.chhaya.rxretrofitmvp.ui.article.mvp;

import android.util.Log;

import androidx.annotation.MainThread;

import com.chhaya.rxretrofitmvp.callback.InteractorResponse;
import com.chhaya.rxretrofitmvp.data.network.ServiceGenerator;
import com.chhaya.rxretrofitmvp.data.network.response.ArticleOneResponse;
import com.chhaya.rxretrofitmvp.data.network.response.ArticleResponse;
import com.chhaya.rxretrofitmvp.data.network.services.ArticleService;

import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleInteractor implements ArticleMvp.Interactor {

    private ArticleService articleService = ServiceGenerator.createService(ArticleService.class);
    private Call<ArticleResponse> articleCall;

    //private RetrofitClient client = RetrofitClient.getInstance();
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void fetch(int page, int limit, InteractorCallback callback) {
        articleCall = articleService.fetch(1, 15);
        articleCall.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ArticleResponse articleResponse = response.body();
                    callback.onSuccess(articleResponse);
                    Log.d("ArticleTag", "Succeed...!");
                } else {
                    Log.d("ArticleTag", "Fail when getting");
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.e("ArticleTag", t.getMessage());
                callback.onError("GGWP WTF");
            }
        });
    }

    @Override
    public void findOne(int id, InteractorCallback callback) {
        /*Single<ArticleOneResponse> single = articleService.findOne(id);
        disposable.add(single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArticleOneResponse>() {
                    @Override
                    public void onSuccess(ArticleOneResponse articleOneResponse) {
                        Log.d("ArticleTag", articleOneResponse.toString());
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ArticleTag", Objects.requireNonNull(e.getMessage()));
                        callback.onError("Error");
                    }
                }));*/
    }

    @Override
    public void onDestroy() {
        disposable.clear();
    }

    @Override
    public void findAll(int page, int limit, InteractorResponse<ArticleResponse> callback) {
        Observable<ArticleResponse> articleResponse = articleService.findAll(page, limit);
        disposable.add(articleResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArticleResponse>() {
                    @Override
                    public void onNext(ArticleResponse articleResponse) {
                        Log.d("ArticleTag", articleResponse.toString());
                        callback.onSuccess(articleResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("ArticleTag", e.getMessage());
                        callback.onError("Sad nas");
                    }

                    @Override
                    public void onComplete() {
                        Log.d("ArticleTag", "Completed ...!");
                    }
                }));
    }
}
