package com.chhaya.rxretrofitmvp.ui.base;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chhaya.rxretrofitmvp.utils.CommonUtils;

public class BaseActivity extends AppCompatActivity implements MvpView
{

    private ProgressBar progressBar;

    @Override
    public void showLoading() {
        //CommonUtils.progressBar(this).setVisibility(View.VISIBLE);
        Log.d("TAG", "DER HZ");
        /*progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleSmall);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);*/
        RelativeLayout layout = new RelativeLayout(this);
        progressBar = new ProgressBar(this,null,android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(progressBar,params);
        setContentView(layout);
    }

    @Override
    public void hideLoading() {
        //CommonUtils.progressBar(this).setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
