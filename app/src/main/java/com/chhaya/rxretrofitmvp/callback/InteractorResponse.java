package com.chhaya.rxretrofitmvp.callback;

import com.chhaya.rxretrofitmvp.data.network.response.ArticleResponse;

import java.util.List;

public interface InteractorResponse<T> {
    void onSuccess(T response);
    void onComplete(String message);
    void onError(String error);
}
